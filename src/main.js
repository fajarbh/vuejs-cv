import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import { FontAwesomeIcon } from "@/plugins/font-awesome";
import './assets/css/tailwind.css'
const app = createApp(App).use(router)
app.component("fa", FontAwesomeIcon)
app.use(store)
app.mount('#app')

