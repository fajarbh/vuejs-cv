import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import {faInstagram, faTwitter,faFacebook,faPinterest,faLinkedin,faGithub} from "@fortawesome/free-brands-svg-icons";
import FontAwesomeIcon from "@/libs/FontAwesomeIcon.vue";
library.add(fas, faTwitter, faInstagram, faFacebook,faPinterest,faLinkedin,faGithub);

export { FontAwesomeIcon };